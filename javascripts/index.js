(async function () {
    if (window.HANEWS) {
        await runHackerNews();
    }

    run();
})();

const TITLES_COUNT = 3;

function run() {
    const source = document.getElementById("source").innerHTML;

    const keyboard = new Keyboard();
    const typing = new Typing(source);

    var firstChar = typing.getNextChar();

    keyboard.pressButton(firstChar);

    var nextLine = typing.getLine();
    var lines = [];

    lines.push(document.getElementById("before"));
    lines.push(document.getElementById("line"));
    lines.push(document.getElementById("after"));

    lines[0].innerHTML = "";
    lines[1].innerHTML = nextLine[0];
    lines[2].innerHTML = nextLine.substring(1);

    document.onkeypress = function(e) {
        var code, nextChar, symbol;

        e = e || window.event;

        code = (e.which ? e.which : e.keyCode);
        symbol = String.fromCharCode(e.which);

        if (typing.tryNext(code)) {
            if (lines[1].innerHTML === "Enter") {
                nextLine = typing.getLine();
                lines[0].innerHTML = "";
                lines[1].innerHTML = nextLine[0];
                lines[2].innerHTML = nextLine.substring(1);
            } else if (lines[2].innerHTML.length === 0) {
                lines[0].innerHTML = "";
                lines[1].innerHTML = "Enter";
                lines[2].innerHTML = "";
            } else {
                lines[0].innerHTML = lines[0].innerHTML + lines[1].innerHTML.replace("&nbsp;", " ");;
                lines[1].innerHTML = lines[2].innerHTML[0].replace(" ", "&nbsp;");
                lines[2].innerHTML = lines[2].innerHTML.substring(1);
            }
        }

        nextChar = typing.getNextChar();

        if (nextChar === false) {
            lines[0].innerHTML = "";
            lines[1].innerHTML = "";
            lines[2].innerHTML = "";

            return document.onkeypress = null;
        }

        keyboard.pressButton(nextChar);
    };
}

async function runHackerNews() {
    const idsRepo = await window.HANEWS.getIds().catch(err => err);
    const itemsRepo = await idsRepo.reduce(idsReducer, Promise.resolve([]));
}

async function idsReducer (res = [], id) {
    const resolvedRes = await res;

    if (resolvedRes.length >= TITLES_COUNT) {
        return res;
    }

    const item = await window.HANEWS.getItem(id).catch(err => err);

    if (isValidItem) {
        resolvedRes.push(item);
        addSource(item.title);
    }

    return resolvedRes;
}

function isValidItem(item) {
    return item && item.title;
}

function putSource(txt) {
    document.getElementById("source").innerHTML = txt;
}

function addSource(txt) {
    document.getElementById("source").innerHTML += '\n' + makePrintable(txt);
}

function makePrintable(txt) {
    txt.replace('–', '-')

    return txt;
}