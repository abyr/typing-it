async function getIds() {
    const topStoriesRequest = new Request('https://hacker-news.firebaseio.com/v0/newstories.json');
    const response = await fetch(topStoriesRequest);
    const json = await response.json();

    return json;
}

async function getItem(id) {
    const itemRequest = new Request(`https://hacker-news.firebaseio.com/v0/item/${id}.json`);
    const response = await fetch(itemRequest);
    const json = await response.json();

    return json;
}

window.HANEWS = {
    getIds,
    getItem
};