(function() {
  var Typing;

  Typing = function(source) {
    this.source = source.replace(/^\s+|\s+$/gm, '');
    this.lines = this.source.match(/[^\r\n]+/g);
    this.index = 0;
    this.lineIndex = 0;
    this.map = {
      "10": "enter",
      "13": "enter",
      "32": "space"
    };
    return this;
  };

  Typing.prototype.setSource = function(source) {
    return this.source = source;
  };

  Typing.prototype.getLine = function() {
    return this.lines[this.lineIndex++] || "";
  };

  Typing.prototype.getNextChar = function() {
    var ch;
    if (this.index >= this.source.length) {
      return false;
    }
    ch = this.source[this.index];
    ch = this.map[this.getNextCode()] || this.source[this.index];
    return ch;
  };

  Typing.prototype.getNextCode = function() {
    var code;
    if (this.index >= this.source.length) {
      return false;
    }
    code = this.source[this.index].charCodeAt();
    if (code === 10) {
      code = 13;
    }
    return code;
  };

  Typing.prototype.tryNext = function(keyCode) {
    if (this.index >= this.source.length) {
      true;
    }
    if (keyCode !== this.getNextCode()) {
      return false;
    }
    this.next();
    return true;
  };

  Typing.prototype.next = function() {
    return ++this.index;
  };

  window.Typing = Typing;

}).call(this);
